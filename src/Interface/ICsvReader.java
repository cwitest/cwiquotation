
package Interface;

/**
 *
 * @author Daniel
 */
public interface ICsvReader {
    void GetCsvLine(String[] lineValues);
}
