package Service;


import Helper.CsvHelper;
import Interface.ICsvReader;
import Model.CoinModel;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 *
 * @author Daniel
 */
public class Quotation implements ICsvReader{
    private ArrayList<CoinModel> coinList;
    private String originPath;
    private CoinModel coinFrom;
    private CoinModel coinTo;
    private String from;
    private String to;
    private String quotation;
    
    public Quotation(String originPath){
        this.originPath = originPath;
    }
    
    public BigDecimal currencyQuotation(String from, String to, Number value) throws Exception{        
        if (quotation == null || quotation.isEmpty() || coinList == null || coinList.isEmpty()){
            throw new Exception("Parameters are not valid!");
        }
        this.from = from;
        this.to = to;
        coinTo = null;
        coinFrom = null;
        
        return currencyQuotation(value);
    }
    
    public BigDecimal currencyQuotation(String from, String to, Number value, String quotation) throws Exception{
        this.from = from;
        this.to = to;
        this.quotation = quotation;
        if (coinList != null){
            coinList.clear();            
        }
        coinTo = null;
        coinFrom = null;
        
        return currencyQuotation(value);
    }
    
    public BigDecimal currencyQuotation(Number value) throws Exception{
        BigDecimal val = new BigDecimal(value.toString());
        if (from == null || from.isEmpty() || from.length() != 3 || to == null || to.isEmpty() || to.length() != 3 || val.doubleValue() < 0){
            throw new Exception("Parameters are not valid!");
        }
        
        if (coinList != null && coinList.size() > 0){
            VerifyCoinList();
        }else{
            coinList = new ArrayList<>();
            if (!new File(originPath).exists()) {
                throw new Exception("Origin path is not valid!");
            }
            Calendar quotationDate = GetQuotationDate();
            String fileName = GetFileName(quotationDate);
            new CsvHelper(originPath).ReadCsvFile(fileName, this);            
        }
        
        return DoConversion(val);
    }
    
    private Calendar GetQuotationDate() throws Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(simpleDateFormat.parse(quotation));
        } catch (ParseException ex) {
            throw new Exception("Quotation is not valid!");
        }
        
        switch (cal.get(Calendar.DAY_OF_WEEK)){
            case Calendar.SATURDAY: cal.add(Calendar.DAY_OF_MONTH, -1);break;
            case Calendar.SUNDAY: cal.add(Calendar.DAY_OF_MONTH, -2);break;
        }
        return cal;
    }
    
    private String GetFileName(Calendar cal) throws Exception{
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        month = month.length() > 1 ? month : "0" + month;
        return String.format("%s%s%s", cal.get(Calendar.YEAR), month, cal.get(Calendar.DAY_OF_MONTH));
    }
    
    private void VerifyCoinList(){
        for (CoinModel coinModel : coinList) {
            SetCoin(coinModel);
            if (coinFrom != null && coinTo != null){
                break;
            }
        }
    }
    
    private void SetCoin(CoinModel coin){
        if (coin.getMoeda().equals(from)){
            coinFrom = coin;
        }else if (coin.getMoeda().equals(to)){
            coinTo = coin;
        }
    }
    
    private BigDecimal DoConversion(BigDecimal value){
        BigDecimal valueInReal = value.multiply(new BigDecimal(coinFrom.getTaxaCompra()));
        return valueInReal.divide(new BigDecimal(coinTo.getTaxaCompra()), 2, RoundingMode.HALF_DOWN);
    }

    @Override
    public void GetCsvLine(String[] lineValues) {
        CoinModel coin = new CoinModel(lineValues);
        SetCoin(coin);
        coinList.add(coin);
    }
}
