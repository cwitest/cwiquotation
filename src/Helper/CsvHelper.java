
package Helper;

import Interface.ICsvReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Daniel
 */
public class CsvHelper {
    private String originPath;
    
    public CsvHelper(String originPath){
        this.originPath = originPath;
    }
    
    public void ReadCsvFile(String name, ICsvReader reader) throws Exception{
        String csvFile = String.format("%s\\%s.csv", originPath, name);
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ";";
	try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
               reader.GetCsvLine(line.split(cvsSplitBy));
            }
	} catch (Exception e) {
            throw new Exception("Does not exists valid source!");
	} finally {
            if (br != null) {
                try {
                        br.close();
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
	}
    }
}
