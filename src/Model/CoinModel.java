package Model;


import java.math.BigDecimal;
import java.util.Date;


/**
 *
 * @author Daniel
 */
public class CoinModel {
    private String Data;
    private String CodMoeda;
    private String Tipo;
    private String Moeda;
    private String TaxaCompra;
    private String TaxaVenda;
    private String ParidadeCompra;
    private String ParidadeVenda;
    
    public CoinModel(String[] csvLine){
        this.Data = csvLine[0];
        this.CodMoeda = csvLine[1];
        this.Tipo = csvLine[2];
        this.Moeda = csvLine[3];
        this.TaxaCompra = csvLine[4].replaceAll(",",".");
        this.TaxaVenda = csvLine[5].replaceAll(",",".");
        this.ParidadeCompra = csvLine[6].replaceAll(",",".");
        this.ParidadeVenda = csvLine[7].replaceAll(",",".");
    }
    
    public String getCodMoeda() {
        return CodMoeda;
    }

    public String getTipo() {
        return Tipo;
    }

    public String getMoeda() {
        return Moeda;
    }

    public String getTaxaCompra() {
        return TaxaCompra;
    }

    public String getTaxaVenda() {
        return TaxaVenda;
    }

    public String getParidadeCompra() {
        return ParidadeCompra;
    }

    public String getParidadeVenda() {
        return ParidadeVenda;
    }
}
